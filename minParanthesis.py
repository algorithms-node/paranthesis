class minParanthesis:
    def __init__(self):
        self.minNumber = 0

    def getMin(self, s):
        lstack, rstack, count = [], [], 0
        sArr = list(s)
        for i, item in enumerate(s):
            if item == '(':
                lstack.append(i)
            if item == ')':
                if len(lstack) > 0:
                    lstack.pop()
                else:
                    rstack.append(i)
        self.minNumber = len(lstack) + len(rstack)
        for i,item in enumerate(lstack):
            sArr[item]=""
        for j,jtem in enumerate(rstack):
            sArr[jtem]=""
        print("".join(sArr))
        return "".join(sArr)

paranthesis = minParanthesis()
# Min paranthesis to remove to make it valid
paranthesis.getMin("((a(bcdefgh(())))")
